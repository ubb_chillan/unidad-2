const Sequelize = require('sequelize')

const {getDatabaseMysql} = require('./mysql');

const sequelize = getDatabaseMysql();

const Colores = sequelize.define('colores', {
    id: {type: Sequelize.SMALLINT, primaryKey: true},
    nombre: Sequelize.STRING,
    rgb: Sequelize.STRING,
  }
);

function agregarColor(color){
  return Colores.create(color);
}

function obtenerLista(){
  return Colores.findAll();
}

module.exports = {
  agregarColor,
  obtenerLista
}
