const Sequelize = require('sequelize')

var databaseMYSQL_HOST = process.env.MYSQL_HOST;
var databaseMYSQL_PORT = process.env.MYSQL_PORT;
var databaseMYSQL_DATABASE = process.env.MYSQL_DATABASE;
var databaseMYSQL_USERNAME = process.env.MYSQL_USERNAME;
var databaseMYSQL_PASSWORD = process.env.MYSQL_PASSWORD;

let sequelize = null;
let conectado = false;

function startDatabase(){
  sequelize = new Sequelize(databaseMYSQL_DATABASE, databaseMYSQL_USERNAME, databaseMYSQL_PASSWORD, {
    host: databaseMYSQL_HOST,
    port: databaseMYSQL_PORT,
    dialect: 'mysql',
  });

  sequelize.authenticate()
  .then(() => {
    conectado = true;
    console.log(`Conectado a ${databaseMYSQL_DATABASE}`)
  })
  .catch(err => {
    console.log('No se conecto')
  })
}

function getDatabaseMysql(){
  if(!sequelize) startDatabase();
  return sequelize;
}

module.exports = {
  getDatabaseMysql,
  startDatabase,
};
