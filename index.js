const express = require('express')
const bodyParser = require('body-parser')
const coloresRouter = require('./routers/colores');

const app = express()

// Transformar a JSON
app.use(bodyParser.json());
app.use('/colores', coloresRouter);

// Pedir la variable de entorno (env)
app.listen(process.env.PUERTO, () => {
  console.log("Escuchando en el puerto " + process.env.PUERTO);
})
