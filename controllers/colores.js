const {
  agregarColor: modelAgregarColor,
  obtenerLista: modelObtenerLista
}  = require('../models/colores');

function agregarColor(color){
  // Llamar a la capa modelo
  return modelAgregarColor(color).then(resultado => {
    return {status: 200, body: {mensaje: resultado}}
  });
}

function obtenerLista(){
  return modelObtenerLista().then(resultado => {
    return {status: 200, body: resultado}
  });
}

module.exports = {
  agregarColor,
  obtenerLista
}
