var express = require('express');
var router = express.Router();

const {
  agregarColor,
  obtenerLista
} = require('../controllers/colores');

router.get('/', function(req, res){
  obtenerLista().then(resultado => {
    res.status(resultado.status);
    res.json(resultado.body);
  });
});

router.post('/', function(req, res){
  agregarColor(req.body).then(resultado => {
    res.status(resultado.status);
    res.json(resultado.body);
  });
});

module.exports = router;
